package Member;

import Memberships.BasicMembership;
import Memberships.GoldMembership;
import Memberships.Membership;
import Memberships.SilverMembership;

import java.time.LocalDate;

/**
 * The type Bonus member.
 */
public class BonusMember {
    // Initializing variables
    private int memberNumber;
    private LocalDate enrolledDate;
    private int bonusPointsBalance = 0;
    private String name;
    private String eMailAddress;
    private String password;

    private Membership membership;

    private static final int SILVER_LIMIT = 25000;
    private static final int GOLD_LIMIT = 75000;

    /**
     * Creating an empty constructor so the Archive.MemberArchive class which inherits from Member.BonusMember
     * doesn't fail.
     */
    public BonusMember() {
    }

    /**
     * Instantiates a new Bonus member.
     *
     * @param memberNumber       the member number
     * @param enrolledDate       the enrolled date
     * @param bonusPointsBalance the bonus points balance
     * @param name               the name
     * @param eMailAddress       the e mail adress
     * @param password           the password
     */
    public BonusMember(int memberNumber, LocalDate enrolledDate, int bonusPointsBalance, String name, String eMailAddress, String password) {
        this.memberNumber = memberNumber;
        this.enrolledDate = enrolledDate;
        this.bonusPointsBalance = bonusPointsBalance;
        this.name = name;
        this.eMailAddress = eMailAddress;
        this.password = password;
        checkAndSetMembership();
    }

    /**
     * Gets member number.
     *
     * @return the member number
     */
    public int getMemberNumber() {
        return memberNumber;
    }

    /**
     * Gets enrolled date.
     *
     * @return the enrolled date
     */
    public LocalDate getEnrolledDate() {
        return enrolledDate;
    }

    /**
     * Gets bonus points balance.
     *
     * @return the bonus points balance
     */
    public int getBonusPointsBalance() {
        return bonusPointsBalance;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets mail adress.
     *
     * @return the mail adress
     */
    public String geteMailAddress() {
        return eMailAddress;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Check a password to see if it matches a password of a user.
     *
     * @param password
     * @return true if password checks out correctly, if not return false.
     */
    public boolean checkPassword(String password) {
        if (this.getPassword().equals(password)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Uses the existing points and adds the new points. Using the method registeredPoints()
     * from the Memberships.Membership class. Also uses checkAndSetMembership() to see which membership
     * the person belongs to after the increase in points.
     *
     * @param newPoints
     */
    public void registerBonusPoints(int newPoints) {
        int newRegisteredPoints = membership.registerPoints(this.bonusPointsBalance, newPoints);

        if (newRegisteredPoints < 0) {
            System.out.println("Not successful");
        } else {
            this.bonusPointsBalance = newRegisteredPoints;
        }
        checkAndSetMembership();
    }

    /**
     * Gets membership level using the point limits.
     * Uses the class which inherits from the Memberships.Membership class
     * to get them their right membership for display.
     */
    private void checkAndSetMembership() {
        if (getBonusPointsBalance() < SILVER_LIMIT) {
            this.membership = new BasicMembership();
        } else if (getBonusPointsBalance() < GOLD_LIMIT) {
            this.membership = new SilverMembership();
        } else if (getBonusPointsBalance() >= GOLD_LIMIT) {
            this.membership = new GoldMembership();
        } else {
            System.out.println("This is not a member.");
        }
    }

    // Using a toString() method to easily print out the Bonusmember.
    @Override
    public String toString() {
        return "Member.BonusMember{" +
                "memberNumber=" + memberNumber +
                ", enrolledDate=" + enrolledDate +
                ", bonusPointsBalance=" + bonusPointsBalance +
                ", name='" + name + '\'' +
                ", eMailAdress='" + eMailAddress + '\'' +
                ", memberLevel='" + this.membership.getMembershipName() + '\'' +
                '}';
    }

}