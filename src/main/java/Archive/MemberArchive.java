package Archive;

import Member.BonusMember;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Scanner;

/**
 * The member archive holds all the bonus members. The archive provides
 * functionality for adding members to the register, looking up bonuspoints
 * of given members, registering new bonuspoints and listing all the members.
 *
 * @author arne
 */
public class MemberArchive extends BonusMember {

    // Use a HashMap, since the members have a unique member number.
    private HashMap<Integer, BonusMember> members;

    // Using a scanner for letting the user input data
    Scanner scanner;

    /**
     * Creates a new instance of Archive.MemberArchive.
     */
    public MemberArchive() {
        this.members = new HashMap<>();
        this.fillRegisterWithTestdata();
        this.scanner = new Scanner(System.in);
    }

    public void addScannerShit(){
        System.out.println("Please enter a membernumber:");
            int memberNumber = scanner.nextInt();
            System.out.println("Please enter the point balance:");
            int bonusPointBalance = scanner.nextInt();
            scanner.nextLine();
            System.out.println("Please enter the name:");
            String name = scanner.nextLine();
            System.out.println("Please enter the email adress:");
            String eMail = scanner.nextLine();
            System.out.println("Please enter a password");
            String password = scanner.nextLine();

        BonusMember bonusMember1 = new BonusMember(memberNumber, LocalDate.now(), bonusPointBalance, name, eMail, password);

        addMember(bonusMember1);
    }

    /**
     * Adds a new member to the register. The new member must have a memebr number
     * different from exsisting members. If not, the new member will not be added.
     *
     * @return {@code true} if the new member was added successfully,
     * {@code false} if the new member could not be added, due to a
     * membernumber that allready exsists.
     */
    public int addMember(BonusMember bonusMember) {
        try {
            if (members.containsKey(bonusMember.getMemberNumber())) {
                System.out.println("This membernumber already exists, so the user was not added.");
                return 0;
            } else {
                members.put(bonusMember.getMemberNumber(), bonusMember);
                System.out.println("Member added successfully.");
            }

        } catch (Exception e) {
            System.out.println("You entered something we didn't recognize.\n" +
                    "Returning to menu...");
        }
        return 1;
    }

    /**
     * This method checks a memberNumber up against a password to see if they match.
     *
     * @param memberNumber
     * @param password
     * @return the point balance if the memberNumber and password match.
     */
    public int findPoints(int memberNumber, String password) {
        if (members.get(memberNumber).checkPassword(password)) {
            return members.get(memberNumber).getBonusPointsBalance();
        } else {
            return -1;
        }
    }

    /**
     * Registers new bonuspoints to the member with the member number given
     * by the parameter {@code memberNumber}. If no member in the register
     * matches the provided member number, {@code false} is returned.
     *
     * @param memberNumber the member number to add the bonus points to
     * @param bonusPoints  the bonus points to be added
     * @return {@code true} if bonuspoints were added successfully,
     * {@code flase} if not.
     */
    public boolean registerPoints(int memberNumber, int bonusPoints) {
        boolean success = false;
        if (memberNumber < 0 || bonusPoints < 0) {
            System.out.println("There is no member with that ID.");
            return false;

        } else if (members.containsKey(memberNumber)) {
            BonusMember member = members.get(memberNumber);
            member.registerBonusPoints(bonusPoints);
            System.out.println("Here are the details:");
            success = true;
        }

        return success;
    }

    /**
     * Lists all members to the console.
     */
    public void listAllMembers() {
        for (BonusMember members : members.values()) {
            System.out.println(members);
        }
    }


    /**
     * Fills the register with some arbitrary members, for testing purposes.
     */
    private void fillRegisterWithTestdata() {
        BonusMember member = new BonusMember(1, LocalDate.now(), 10000, "Olsen, Ole", "ole@olsen.biz", "123");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(2, LocalDate.now(), 15000, "Jensen, Jens", "jens@jensen.biz", "111");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(3, LocalDate.now(), 5000, "Lie, Linda", "linda@lie.no", "161");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(4, LocalDate.now(), 30000, "Paulsen, Paul", "paul@paulsen.org", "112");
        this.members.put(member.getMemberNumber(), member);
        member = new BonusMember(5, LocalDate.now(), 75000, "FLo, Finn", "finn.flo@gmail.com", "009");
        this.members.put(member.getMemberNumber(), member);

    }
}
