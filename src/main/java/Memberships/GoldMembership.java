package Memberships;

/**
 * The type Gold membership.
 */
public class GoldMembership extends Membership {
    private final float POINTS_SCALING_FACTOR_LEVEL_1 = 1.3f;
    private final float POINTS_SCALING_FACTOR_LEVEL_2 = 1.5f;

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        if (bonusPointBalance < 90000) {
            return bonusPointBalance + Math.round(POINTS_SCALING_FACTOR_LEVEL_1 * newPoints);
        } else if (bonusPointBalance >= 90000) {
            return bonusPointBalance + Math.round(POINTS_SCALING_FACTOR_LEVEL_2 * newPoints);
        }
        return 0;
    }

    @Override
    public String getMembershipName() {
        return "Gold";
    }
}
