package Memberships;

/**
 * The type Memberships.Membership.
 */
public abstract class Membership {

    /**
     * Register points to the user.
     *
     * @param bonusPointBalance
     * @param newPoints
     * @return the int which will be the point balance + the new points.
     */
    public abstract int registerPoints(int bonusPointBalance, int newPoints);

    /**
     * Gets membership name.
     *
     * @return the membership name
     */
    public abstract String getMembershipName();
}
