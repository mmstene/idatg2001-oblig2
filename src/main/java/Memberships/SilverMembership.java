package Memberships;

/**
 * The type Silver membership.
 */
public class SilverMembership extends Membership {

    private final float POINTS_SCALING_FACTOR = 1.2f;

    @Override
    public int registerPoints(int bonusPointBalance, int newPoints) {
        return bonusPointBalance + Math.round(POINTS_SCALING_FACTOR * newPoints);
    }

    @Override
    public String getMembershipName() {
        return "Silver";
    }
}
